/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.elevationdrag;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.support.v4.app.ActivityCompat.startActivity;

/**
 * A {@link FrameLayout} that allows the user to drag and reposition child views.
 */
public class DragFrameLayout extends FrameLayout {


    private static final int DEFAULT_SQUARE_PADDING = 65 ;
    public final static String EXTRA_MESSAGE = "com.example.android.elevationdrag.MESSAGE";

    /**
     * The list of {@link View}s that will be draggable.
     */
    private List<View> mDragViews;

    /**
     * The {@link DragFrameLayoutController} that will be notify on drag.
     */
    private DragFrameLayoutController mDragFrameLayoutController;

    private ViewDragHelper mDragHelper;
    private View BoxView,RedV,GreenV,BlueV,CircleView,GreenSet,RedSet,BlueSet,RainbowSet,Win,Menu,Restart,Next;
    private View BoxView2,BoxView3,BoxView4,BoxViewC;
    private int WS;
    public static boolean testwin=false;

    public DragFrameLayout(Context context) {
        this(context, null, 0, 0);
    }

    public DragFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public DragFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public DragFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mDragViews = new ArrayList<View>();

        /**
         * Create the {@link ViewDragHelper} and set its callback.
         */

        mDragHelper = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {

//            int x1;
//            int y1;
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return mDragViews.contains(child);
            }

//            @Override
//            public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
//                //super.onViewPositionChanged(changedView, left, top, dx, dy);
//            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {
                return left;
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                return top;
            }
//            @Override
//            public void onViewCaptured(View capturedChild, int activePointerId) {
//                 super.onViewCaptured(capturedChild, activePointerId);
//                x1 = (int) capturedChild.getX();
//                y1 = (int) capturedChild.getY();
//                if (mDragFrameLayoutController != null) {
//                    mDragFrameLayoutController.onDragDrop(true);
//                }
//            }


//            @Override
//            public void onViewReleased(View releasedChild, float xvel, float yvel) {
////               releasedChild.setX(x1);
////                releasedChild.setY(y1);
//
//               //releasedChild.setPadding(x1,y1,0,0);
//                x1 = (int) releasedChild.getX();
//                y1 = (int) releasedChild.getY();
////                clampViewPositionVertical(releasedChild, y1, y1);
////                clampViewPositionHorizontal(releasedChild, x1, x1);
//                super.onViewReleased(releasedChild,xvel, yvel);
//
//
//
//                //                releasedChild.setLeft(x1);
////                releasedChild.setTop(y1);
//
//
//               // mDragHelper.smoothSlideViewTo(releasedChild,x1,y1);
//              //while (mDragHelper.continueSettling(false));
//                if (mDragFrameLayoutController != null) {
////                    releasedChild.setX(x1);
////                     releasedChild.setY(y1);
//                    mDragFrameLayoutController.onDragDrop(false); // extra element ))
//
//                }
//            }
        });
    }

    int xRec=-1;
    int yRec=-1;
    int xRec2=-1;
    int yRec2=-1;
    public static final String TL= "Test_Line";
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        final int action = ev.getActionMasked();
//
//        if(action == MotionEvent.ACTION_DOWN) {
//            xRec=(int)ev.getRawX();
//            yRec=(int)ev.getRawY();
//
//            //Log.v(TT,"sds");
//        }
//            if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
//                Log.v(TT,"sdsd");
//            //return false;
//        }
//        return mDragHelper.shouldInterceptTouchEvent(ev);
//    }



    View next;
    View next2,next3=null,next4=null;
    ColorStateList BXCSL,BXCSL2;
    ColorDrawable BoxColorDrawable;
    int BoxcolorId,redc,greenc,bluec;
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
            mDragHelper.processTouchEvent(ev);

        final int action2 = ev.getActionMasked();
        if (testwin == false) {
            next = null;
//        if( ((int)ev.getX()>(int)RedV.getLeft())&&((int)ev.getX()<(int)RedV.getRight())&&((int)ev.getY()>(int)RedV.getTop())&&((int)ev.getX()<(int)RedV.getBottom()) )
//        {next=RedV;
//            mDragHelper.captureChildView(RedSet, 0);
//            next3=mDragHelper.getCapturedView();}
//        if( ((int)ev.getX()>(int)BlueV.getLeft())&&((int)ev.getX()<(int)BlueV.getRight())&&((int)ev.getY()>(int)BlueV.getTop())&&((int)ev.getX()<(int)BlueV.getBottom()) )
//        {next=BlueV;
//            mDragHelper.captureChildView(BlueSet, 0);
//            next3=mDragHelper.getCapturedView();}
//        if( ((int)ev.getX()>(int)GreenV.getLeft())&&((int)ev.getX()<(int)GreenV.getRight())&&((int)ev.getY()>(int)GreenV.getTop())&&((int)ev.getX()<(int)GreenV.getBottom()) )
//        {next=GreenV;
//            mDragHelper.captureChildView(GreenSet, 0);
//            next3=mDragHelper.getCapturedView();
//        }
//        if( ((int)ev.getX()-(int)BoxView.getX()-BoxView.getHeight()/2)*((int)ev.getX()-(int)BoxView.getX()-BoxView.getHeight()/2)+((int)ev.getY()-(int)BoxView.getY()-BoxView.getHeight()/2)*((int)ev.getY()-(int)BoxView.getY()-BoxView.getHeight()/2)<=(BoxView.getHeight()/2)*(BoxView.getHeight()/2) )
//        {next=BoxView;
//            mDragHelper.captureChildView(RainbowSet, 0);
//            next3=mDragHelper.getCapturedView();}
            //next=mDragHelper. getCapturedView();
            if (action2 == MotionEvent.ACTION_DOWN) {


                if (((int) ev.getX() > (int) RedV.getLeft()) && ((int) ev.getX() < (int) RedV.getRight()) && ((int) ev.getY() > (int) RedV.getTop()) && ((int) ev.getX() < (int) RedV.getBottom())) {
                    next = RedV;
                    mDragHelper.captureChildView(RedSet, 0);
                    next3 = mDragHelper.getCapturedView();
                }
                if (((int) ev.getX() > (int) BlueV.getLeft()) && ((int) ev.getX() < (int) BlueV.getRight()) && ((int) ev.getY() > (int) BlueV.getTop()) && ((int) ev.getX() < (int) BlueV.getBottom())) {
                    next = BlueV;
                    mDragHelper.captureChildView(BlueSet, 0);
                    next3 = mDragHelper.getCapturedView();
                }
                if (((int) ev.getX() > (int) GreenV.getLeft()) && ((int) ev.getX() < (int) GreenV.getRight()) && ((int) ev.getY() > (int) GreenV.getTop()) && ((int) ev.getX() < (int) GreenV.getBottom())) {
                    next = GreenV;
                    mDragHelper.captureChildView(GreenSet, 0);
                    next3 = mDragHelper.getCapturedView();
                }
                if (((int) ev.getX() - (int) BoxViewC.getX() - BoxViewC.getHeight() / 2) * ((int) ev.getX() - (int) BoxViewC.getX() - BoxViewC.getHeight() / 2) + ((int) ev.getY() - (int) BoxViewC.getY() - BoxViewC.getHeight() / 2) * ((int) ev.getY() - (int) BoxViewC.getY() - BoxViewC.getHeight() / 2) <= (BoxViewC.getHeight() / 2) * (BoxViewC.getHeight() / 2)) {

                    double f_cos = (ev.getX() - (BoxViewC.getLeft() + (BoxViewC.getWidth() / 2))) / Math.sqrt(Math.pow(ev.getX() - (BoxViewC.getLeft() + (BoxViewC.getWidth() / 2)), 2) + Math.pow(ev.getY() - (BoxViewC.getTop() + (BoxViewC.getHeight() / 2)), 2));
                    double f_sin = (ev.getY() - (BoxViewC.getTop() + (BoxViewC.getHeight() / 2))) / Math.sqrt(Math.pow(ev.getX() - (BoxViewC.getLeft() + (BoxViewC.getWidth() / 2)), 2) + Math.pow(ev.getY() - (BoxViewC.getTop() + (BoxViewC.getHeight() / 2)), 2));
                    double f_acos = Math.toDegrees(Math.acos(f_cos));
                    double f_asin = Math.toDegrees(Math.asin(f_sin));
                    double f = 0, f1, f2, f3, f4, fn1, fn2, fn3, fn4;
                    if ((f_sin > 0) && (f_cos > 0)) f = (f_asin + 90) % 360;
                    if ((f_sin > 0) && (f_cos < 0)) f = (f_acos + 90) % 360;
                    if ((f_sin < 0) && (f_cos > 0)) f = (f_asin + 360 + 90) % 360;
                    if ((f_sin < 0) && (f_cos < 0)) f = (360 - f_acos + 90) % 360;
                    f1 = BoxView.getRotation();
                    f2 = BoxView2.getRotation();
                    f3 = BoxView3.getRotation();
                    f4 = BoxView4.getRotation();
                    fn1 = Gradus(f1);
                    fn2 = Gradus(f2);
                    fn3 = Gradus(f3);
                    fn4 = Gradus(f4);

                    if (f1 > fn1) {
                        if (f < f1 && f > fn1) {
                            next = BoxView;
                            mDragHelper.captureChildView(RainbowSet, 0);
                            next3 = mDragHelper.getCapturedView();
                        }
                    } else {
                        if (f < f1 || f > fn1) {
                            next = BoxView;
                            mDragHelper.captureChildView(RainbowSet, 0);
                            next3 = mDragHelper.getCapturedView();
                        }
                    }


                    if (f2 > fn2) {
                        if (f < f2 && f > fn2) {
                            next = BoxView2;
                            mDragHelper.captureChildView(RainbowSet, 0);
                            next3 = mDragHelper.getCapturedView();
                        }
                    } else {
                        if (f < f2 || f > fn2) {
                            next = BoxView2;
                            mDragHelper.captureChildView(RainbowSet, 0);
                            next3 = mDragHelper.getCapturedView();
                        }
                    }
                    //Log.v(TL, " " + f + " " + ((int)BoxView.getRotation()/360)*360);

                    if (f3 > fn3) {
                        if (f < f3 && f > fn3) {
                            next = BoxView3;
                            mDragHelper.captureChildView(RainbowSet, 0);
                            next3 = mDragHelper.getCapturedView();
                        }
                    } else {
                        if (f < f3 || f > fn3) {
                            next = BoxView3;
                            mDragHelper.captureChildView(RainbowSet, 0);
                            next3 = mDragHelper.getCapturedView();
                        }
                    }


                    if (f4 > fn4) {
                        if (f < f4 && f > fn4) {
                            next = BoxView4;
                            mDragHelper.captureChildView(RainbowSet, 0);
                            next3 = mDragHelper.getCapturedView();
                        }
                    } else {
                        if (f < f4 || f > fn4) {
                            next = BoxView4;
                            mDragHelper.captureChildView(RainbowSet, 0);
                            next3 = mDragHelper.getCapturedView();
                        }
                    }
                }
                //Log.v(TL, "DOWN");
                next2 = next;
                if (next3 != null) {
                    next3.setAlpha(1);
                    next3.setX((int) ev.getX() - DEFAULT_SQUARE_PADDING);
                    next3.setY((int) ev.getY() - DEFAULT_SQUARE_PADDING);
                }
//            next3.setMinimumHeight(100);
//            next3.setMinimumWidth(100);
//            xRec=(int)next2.getX();
//            yRec=(int)next2.getY();
            }

//        if ((action2 == MotionEvent.ACTION_CANCEL || action2 == MotionEvent.ACTION_UP)&& xRec != -1)
            if ((action2 == MotionEvent.ACTION_CANCEL || action2 == MotionEvent.ACTION_UP)) {
                if (next3 != null) {
                    next3.setAlpha(0);
                }
                next3 = null;
                //Log.v(TL, "UP");
//            xRec2=next2.getWidth();
//            yRec2=next2.getHeight();
//            next2.setTop(yRec);
//            next2.setLeft(xRec);
//            next2.setBottom(yRec + yRec2);
//            next2.setRight(xRec + xRec2);
                if ((next2 == BoxView) || (next2 == BoxView2) || (next2 == BoxView3) || (next2 == BoxView4)) {
                    BXCSL = (ColorStateList) next2.getBackgroundTintList();
                    //BoxColorDrawable = (ColorDrawable) BoxView.getBackground();
                    //BoxcolorId = BoxColorDrawable.getColor();
                    BoxcolorId = BXCSL.getDefaultColor();
                    redc = Color.red(BoxcolorId);
                    greenc = Color.green(BoxcolorId);
                    bluec = Color.blue(BoxcolorId);

                    if (((int) ev.getX() > (int) GreenV.getLeft()) && ((int) ev.getX() < (int) GreenV.getRight()) && ((int) ev.getY() > (int) GreenV.getTop()) && ((int) ev.getX() < (int) GreenV.getBottom())) {
                        if ((greenc == 0) || (greenc == 31)) {
                            greenc = 32;
                        }
                        next2.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redc, greenc - 32, bluec)));
                    }
                    if (((int) ev.getX() > (int) BlueV.getLeft()) && ((int) ev.getX() < (int) BlueV.getRight()) && ((int) ev.getY() > (int) BlueV.getTop()) && ((int) ev.getX() < (int) BlueV.getBottom())) {
                        if ((bluec == 0) || (bluec == 31)) {
                            bluec = 32;
                        }
                        next2.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redc, greenc, bluec - 32)));
                    }
                    if (((int) ev.getX() > (int) RedV.getLeft()) && ((int) ev.getX() < (int) RedV.getRight()) && ((int) ev.getY() > (int) RedV.getTop()) && ((int) ev.getX() < (int) RedV.getBottom())) {
                        if ((redc == 0) || (redc == 31)) {
                            redc = 32;
                        }
                        next2.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redc - 32, greenc, bluec)));
                    }

                    testwin = ColorTestDrive(BoxView, BoxView4, BoxView2, BoxView3, redc2, greenc2, bluec2);
                    if (testwin == true) {

                        // Log.v(TL, "Win");
                        Win.setAlpha(1);
                        Next.setAlpha(1);
                        Restart.setAlpha(1);
                        Menu.setAlpha(1);
                        Next.setClickable(true);
                        Restart.setClickable(true);
                        Menu.setClickable(true);


                    }
                    next2 = null;
                }
                if (next2 == GreenV || next2 == BlueV || next2 == RedV) {
                    if (((int) ev.getX() - (int) BoxViewC.getX() - BoxViewC.getHeight() / 2) * ((int) ev.getX() - (int) BoxViewC.getX() - BoxViewC.getHeight() / 2) + ((int) ev.getY() - (int) BoxViewC.getY() - BoxViewC.getHeight() / 2) * ((int) ev.getY() - (int) BoxViewC.getY() - BoxViewC.getHeight() / 2) <= (BoxViewC.getHeight() / 2) * (BoxViewC.getHeight() / 2))
                    //if(((int)ev.getX()>BoxView.getLeft())&&((int)ev.getY()>BoxView.getTop())&&((int)ev.getX()<BoxView.getRight())&&((int)ev.getY()<BoxView.getBottom()))
                    {
                        double f_cos = (ev.getX() - (BoxViewC.getLeft() + (BoxViewC.getWidth() / 2))) / Math.sqrt(Math.pow(ev.getX() - (BoxViewC.getLeft() + (BoxViewC.getWidth() / 2)), 2) + Math.pow(ev.getY() - (BoxViewC.getTop() + (BoxViewC.getHeight() / 2)), 2));
                        double f_sin = (ev.getY() - (BoxViewC.getTop() + (BoxViewC.getHeight() / 2))) / Math.sqrt(Math.pow(ev.getX() - (BoxViewC.getLeft() + (BoxViewC.getWidth() / 2)), 2) + Math.pow(ev.getY() - (BoxViewC.getTop() + (BoxViewC.getHeight() / 2)), 2));
                        double f_acos = Math.toDegrees(Math.acos(f_cos));
                        double f_asin = Math.toDegrees(Math.asin(f_sin));
                        double f = 0, f1, f2, f3, f4, fn1, fn2, fn3, fn4;
                        if ((f_sin > 0) && (f_cos > 0)) f = (f_asin + 90) % 360;
                        if ((f_sin > 0) && (f_cos < 0)) f = (f_acos + 90) % 360;
                        if ((f_sin < 0) && (f_cos > 0)) f = (f_asin + 360 + 90) % 360;
                        if ((f_sin < 0) && (f_cos < 0)) f = (360 - f_acos + 90) % 360;
                        f1 = BoxView.getRotation();
                        f2 = BoxView2.getRotation();
                        f3 = BoxView3.getRotation();
                        f4 = BoxView4.getRotation();
                        fn1 = Gradus(f1);
                        fn2 = Gradus(f2);
                        fn3 = Gradus(f3);
                        fn4 = Gradus(f4);

                        if (f1 > fn1) {
                            if (f < f1 && f > fn1) {
                                next4 = BoxView;
                            }
                        } else {
                            if (f < f1 || f > fn1) {
                                next4 = BoxView;
                            }
                        }


                        if (f2 > fn2) {
                            if (f < f2 && f > fn2) {
                                next4 = BoxView2;
                            }
                        } else {
                            if (f < f2 || f > fn2) {
                                next4 = BoxView2;
                            }
                        }
                        //Log.v(TL, " " + f + " " + ((int)BoxView.getRotation()/360)*360);

                        if (f3 > fn3) {
                            if (f < f3 && f > fn3) {
                                next4 = BoxView3;
                            }
                        } else {
                            if (f < f3 || f > fn3) {
                                next4 = BoxView3;
                            }
                        }


                        if (f4 > fn4) {
                            if (f < f4 && f > fn4) {
                                next4 = BoxView4;
                            }
                        } else {
                            if (f < f4 || f > fn4) {
                                next4 = BoxView4;
                            }
                        }

                        BXCSL = (ColorStateList) next4.getBackgroundTintList();
                        //BoxColorDrawable = (ColorDrawable) BoxView.getBackground();
                        //BoxcolorId = BoxColorDrawable.getColor();
                        BoxcolorId = BXCSL.getDefaultColor();
                        redc = Color.red(BoxcolorId);
                        greenc = Color.green(BoxcolorId);
                        bluec = Color.blue(BoxcolorId);

                        //BoxView.setBackgroundColor(Color.rgb((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
                        if (next2 == BlueV) {
                            if (bluec == 255) {
                                bluec = 223;
                            }
                            if (bluec == 0) {
                                bluec = -1;
                            }
                            next4.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redc, greenc, bluec + 32)));
                        }
                        if (next2 == RedV) {
                            if (redc == 255) {
                                redc = 223;
                            }
                            if (redc == 0) {
                                redc = -1;
                            }
                            next4.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redc + 32, greenc, bluec)));
                        }
                        if (next2 == GreenV) {
                            if (greenc == 255) {
                                greenc = 223;
                            }
                            if (greenc == 0) {
                                greenc = -1;
                            }
                            next4.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redc, greenc + 32, bluec)));
                        }
//                if(next2==RedV)
//                {
//                    if(redc-20<0)
//                    {bluec=20;}
//                    BoxView.setBackgroundColor(Color.rgb(redc-20, greenc,bluec));
//                }
                        testwin = ColorTestDrive(BoxView, BoxView4, BoxView2, BoxView3, redc2, greenc2, bluec2);
                        if (testwin == true) {

                            // Log.v(TL, "Win");
                            Win.setAlpha(1);
                            Next.setAlpha(1);
                            Restart.setAlpha(1);
                            Menu.setAlpha(1);
                            Next.setClickable(true);
                            Restart.setClickable(true);
                            Menu.setClickable(true);


                        }
                        next2 = null;
                        next4 = null;
                    }
                }
                //next2.setY(yRec);
                //next2.setX(xRec);
                xRec = -1;
                yRec = -1;
                //return false;
            }
        }

                return true;
            }
    private boolean ColorTestDrive(View boxView, View boxView4, View boxView2, View boxView3, int redC2, int greenC2, int blueC2) {
        int k=0;
        ColorStateList BXCSLt= (ColorStateList)boxView.getBackgroundTintList();
        //BoxColorDrawable = (ColorDrawable) BoxView.getBackground();
        //BoxcolorId = BoxColorDrawable.getColor();
        int BoxcolorIdt = BXCSLt.getDefaultColor();
        int redct=Color.red(BoxcolorIdt);
        int greenct=Color.green(BoxcolorIdt);
        int bluect=Color.blue(BoxcolorIdt);

        if((redct==redC2)&&(greenct==greenC2)&&(bluect==blueC2))
        {k++;}

        BXCSLt= (ColorStateList)boxView2.getBackgroundTintList();
        //BoxColorDrawable = (ColorDrawable) BoxView.getBackground();
        //BoxcolorId = BoxColorDrawable.getColor();
        BoxcolorIdt = BXCSLt.getDefaultColor();
        redct=Color.red(BoxcolorIdt);
        greenct=Color.green(BoxcolorIdt);
        bluect=Color.blue(BoxcolorIdt);

        if((redct==redC2)&&(greenct==greenC2)&&(bluect==blueC2))
        {k++;}

        BXCSLt= (ColorStateList)boxView3.getBackgroundTintList();
        //BoxColorDrawable = (ColorDrawable) BoxView.getBackground();
        //BoxcolorId = BoxColorDrawable.getColor();
        BoxcolorIdt = BXCSLt.getDefaultColor();
        redct=Color.red(BoxcolorIdt);
        greenct=Color.green(BoxcolorIdt);
        bluect=Color.blue(BoxcolorIdt);

        if((redct==redC2)&&(greenct==greenC2)&&(bluect==blueC2))
        {k++;}

        BXCSLt= (ColorStateList)boxView4.getBackgroundTintList();
        //BoxColorDrawable = (ColorDrawable) BoxView.getBackground();
        //BoxcolorId = BoxColorDrawable.getColor();
        BoxcolorIdt = BXCSLt.getDefaultColor();
        redct=Color.red(BoxcolorIdt);
        greenct=Color.green(BoxcolorIdt);
        bluect=Color.blue(BoxcolorIdt);

        if((redct==redC2)&&(greenct==greenC2)&&(bluect==blueC2))
        {k++;}

        if(k==4) return true;else return false;
    }

    private double Gradus(double ner) {
        if(ner>90) return ner-90;
        else return 360+ner-90;
    }


//    @Override
//    public boolean  onDragEvent (DragEvent ev){
//
//        if( ev.getAction()==DragEvent. ACTION_DRAG_STARTED) {
//            xRec=(int)ev.getX();
//            yRec=(int)ev.getX();
//            Log.v(TT,"DRAG_STARTED");
//        }
//        if(ev.getAction()==DragEvent.ACTION_DROP) {
//            mDragHelper.settleCapturedViewAt(xRec,yRec);
//            Log.v(TT,"DROP");
//        }
//        Log.v(TT,"help");
//        return true;
//    }




        /**
         * Adds a new {@link View} to the list of views that are draggable within the container.
         * @param dragView the {@link View} to make draggable
         */
    public void addDragView(View dragView) {
        //mDragViews.=dragView;
        mDragViews.add(dragView);
    }

    public void addBox2(View dragView) {
        BoxView2=dragView;
        int redct2=Color.red((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256*256);
        int greenct2=Color.green((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256);
        int bluect2=Color.blue((((int)Math.round(((Math.random() * 7)+1)))*32-1));
        BoxView2.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redct2,greenct2,bluect2)));
        //BoxView2.setBackgroundTintList(ColorStateList.valueOf(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255))));
        BoxView2.setRotation(BoxView2.getRotation() + 90);

    }

    public void addBox3(View dragView) {
        BoxView3=dragView;
        int redct2=Color.red((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256*256);
        int greenct2=Color.green((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256);
        int bluect2=Color.blue((((int)Math.round(((Math.random() * 7)+1)))*32-1));
        BoxView3.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redct2,greenct2,bluect2)));
        BoxView3.setRotation(BoxView3.getRotation() + 180);
    }

    public void addBox4(View dragView) {
        BoxView4=dragView;
        int redct2=Color.red((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256*256);
        int greenct2=Color.green((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256);
        int bluect2=Color.blue((((int)Math.round(((Math.random() * 7)+1)))*32-1));
        BoxView4.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redct2,greenct2,bluect2)));
        //BoxView4.setBackgroundTintList(ColorStateList.valueOf(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255))));
        BoxView4.setRotation(BoxView4.getRotation() + 270);
    }

    public void addBoxC(View dragView) {
        BoxViewC=dragView;

    }



    public void addBox(View dragView) {
        BoxView=dragView;
        int redct2=Color.red((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256*256);
        int greenct2=Color.green((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256);
        int bluect2=Color.blue((((int)Math.round(((Math.random() * 7)+1)))*32-1));
        BoxView.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redct2,greenct2,bluect2)));
        //BoxView.setBackgroundTintList(ColorStateList.valueOf(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255))));
       // BoxView.setBackgroundColor(Color.rgb((int) (Math.random() * 50), (int) (Math.random() * 255), (int) (Math.random() * 150)));
    }



    int redc2,greenc2,bluec2;
    public void addCircle(View dragView) {
        CircleView=dragView;
       // CircleView.setBackgroundColor(Color.rgb((int) (Math.random() * 100)+154, (int) (Math.random() * 100)+154,(int) (Math.random() * 150)+104));
       // CircleView.setBackgroundTintList(ColorStateList.valueOf(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255))));
        int redct2=Color.red((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256*256);
        int greenct2=Color.green((((int)Math.round(((Math.random() * 7)+1)))*32-1)*256);
        int bluect2=Color.blue((((int)Math.round(((Math.random() * 7)+1)))*32-1));
        CircleView.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(redct2,greenct2,bluect2)));

        BXCSL2= (ColorStateList)CircleView.getBackgroundTintList();
        //BoxColorDrawable = (ColorDrawable) BoxView.getBackground();
        //BoxcolorId = BoxColorDrawable.getColor();
        int BoxcolorId2 = BXCSL2.getDefaultColor();

        redc2=Color.red(BoxcolorId2);
        greenc2=Color.green(BoxcolorId2);
        bluec2=Color.blue(BoxcolorId2);
    }

    public void addGreenSet(View dragView) {
        GreenSet=dragView;
    }

    public void addRainbowSet(View dragView) {
        RainbowSet=dragView;
    }

    public void addRedSet(View dragView) {
        RedSet=dragView;
    }
    public void addBlueSet(View dragView) {
        BlueSet=dragView;
    }

    public void addRedV(View dragView) {
        RedV=dragView;
    }
    public void addText(View dragView) {
        Win=dragView;
    }


    public void addGreenV(View dragView) {
        GreenV=dragView;
    }
    public void addBlueV(View dragView) {
        BlueV=dragView;
    }

    public void addWinS(int winSearch) {
        WS=winSearch;
    }

    public void addMenu(View dragView) {
        Menu=dragView;
    }

    public void addNext(View dragView) {
        Next=dragView;
    }

    public void addRestart(View dragView) {
        Restart=dragView;
    }


    //RedMinus,RedPlus,GreenMinus,GreenPlus,BlueMinus,BluePlus;
    /**
     * Sets the {@link DragFrameLayoutController} that will receive the drag events.
     * @param dragFrameLayoutController a {@link DragFrameLayoutController}
     */
//    public void setDragFrameController(DragFrameLayoutController dragFrameLayoutController) {
//        mDragFrameLayoutController = dragFrameLayoutController;
//    }

    /**
     * A controller that will receive the drag events.
     */
    public interface DragFrameLayoutController {

        public void onDragDrop(boolean captured);
    }
}
