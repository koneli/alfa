/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.elevationdrag;

import com.example.android.common.logger.Log;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Outline;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.TextView;

import java.sql.Date;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class ElevationDragFragment extends Fragment {

   public static final String TAG = "ElevationDragFragment";

    /* The circular outline provider */
    private ViewOutlineProvider mOutlineProviderCircle;

    /* The current elevation of the floating view. */
    //private float mElevation = 0;

    /* The step in elevation when changing the Z value */
   // private int mElevationStep;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mOutlineProviderCircle = new CircleOutlineProvider();

       // mElevationStep = getResources().getDimensionPixelSize(R.dimen.elevation_step);
    }
 public boolean mTestTimer=true;
    Timer mtimer2;
    TimerTask BoxTimer;
    View floatingBox,floatingBox2,floatingBox3,floatingBox4,WinText,Next,Menu,Restart;
    int WinSearch=0,k=0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ztranslation, container, false);

        /* Find the {@link View} to apply z-translation to. */
        final View floatingBoxC = rootView.findViewById(R.id.box2);
        floatingBox = rootView.findViewById(R.id.box);
        floatingBox2 = rootView.findViewById(R.id.box_2);
        floatingBox3 = rootView.findViewById(R.id.box_3);
        floatingBox4 = rootView.findViewById(R.id.box_4);
        final View floatingCircle = rootView.findViewById(R.id.circle);
        final View floatingShape5  = rootView.findViewById(R.id.box_minus_green);
        final View floatingShape6 = rootView.findViewById(R.id.box_minus_blue);
        final View floatingShape7 = rootView.findViewById(R.id.box_minus_red);
        final View floatingShape8= rootView.findViewById(R.id.vgreen);
        final View floatingShape9= rootView.findViewById(R.id.vred);
        final View floatingShape1= rootView.findViewById(R.id.vblue);
        final View floatingShape0= rootView.findViewById(R.id.vrainbow);
        WinText = rootView.findViewById(R.id.Win);
        Next = rootView.findViewById(R.id.Next);
        Menu = rootView.findViewById(R.id.Menu);
        Restart = rootView.findViewById(R.id.Restart);

//        floatingBox.setBackgroundTintList(ColorStateList.valueOf(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255))));
//        floatingCircle.setBackgroundTintList(ColorStateList.valueOf(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255))));
//        floatingBox2.setBackgroundTintList(ColorStateList.valueOf(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255))));
//       floatingBox3.setBackgroundTintList(ColorStateList.valueOf(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255))));
//        floatingBox4.setBackgroundTintList(ColorStateList.valueOf(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255))));
        /* Define the shape of the {@link View}'s shadow by setting one of the {@link Outline}s. */
        floatingBox.setOutlineProvider(mOutlineProviderCircle);
        floatingCircle.setOutlineProvider(mOutlineProviderCircle);
//        floatingShape3.setOutlineProvider(mOutlineProviderCircle);
//        floatingShape4.setOutlineProvider(mOutlineProviderCircle);

        /* Clip the {@link View} with its outline. */
        floatingBox.setClipToOutline(true);
        floatingCircle.setClipToOutline(true);
//        floatingShape3.setClipToOutline(true);
//        floatingShape4.setClipToOutline(true);

        DragFrameLayout dragLayout = ((DragFrameLayout) rootView.findViewById(R.id.main_layout));

//        dragLayout.setDragFrameController(new DragFrameLayout.DragFrameLayoutController() {
//
//            @Override
//            public void onDragDrop(boolean captured) {
//                /* Animate the translation of the {@link View}. Note that the translation
//                 is being modified, not the elevation. */
//                floatingShape.animate()
//                        .translationZ(captured ? 30 : 0)
//                        .setDuration(100);
//                floatingShape2.animate()
//                        .translationZ(captured ? 30 : 0)
//                        .setDuration(100);
//                floatingShape3.animate()
//                        .translationZ(captured ? 30 : 0)
//                        .setDuration(100);
//                floatingShape4.animate()
//                        .translationZ(captured ? 30 : 0)
//                        .setDuration(100);
//                // Log.d(TAG, captured ? "Drag" : "Drop");
//            }
//        });
        floatingShape8.setAlpha(0);
        floatingShape9.setAlpha(0);
        floatingShape1.setAlpha(0);
        floatingShape0.setAlpha(0);
        WinText.setAlpha(0);
        Next.setAlpha(0);
        Restart.setAlpha(0);
        Menu.setAlpha(0);
        Next.setClickable(false);
        Restart.setClickable(false);
        Menu.setClickable(false);
//        floatingBox2.setRotation(floatingBox2.getRotation() + 90);
//        floatingBox3.setRotation(floatingBox3.getRotation() + 180);
//        floatingBox4.setRotation(floatingBox4.getRotation() + 270);
//        class firstTask extends TimerTask {
//
//            @Override
//            public void run() {
//                floatingBox.setRotation(floatingBox.getRotation()+1);
//                if(floatingBox.getRotation()==360)floatingBox.setRotation(floatingBox.getRotation()%360);
//                floatingBox2.setRotation(floatingBox2.getRotation()+1);
//                if(floatingBox2.getRotation()==360)floatingBox2.setRotation(floatingBox2.getRotation()%360);
//                floatingBox3.setRotation(floatingBox3.getRotation()+1);
//                if(floatingBox3.getRotation()==360)floatingBox3.setRotation(floatingBox3.getRotation()%360);
//                floatingBox4.setRotation(floatingBox4.getRotation()+1);
//                if(floatingBox4.getRotation()==360)floatingBox4.setRotation(floatingBox4.getRotation()%360);
//            }
//        };
//        mtimer2=new Timer();
//        BoxTimer= new firstTask();
//        mtimer2.schedule(BoxTimer,50,50);

        dragLayout.addBoxC(floatingBoxC);
        dragLayout.addBox(floatingBox);
        dragLayout.addBox2(floatingBox2);
        dragLayout.addBox3(floatingBox3);
        dragLayout.addBox4(floatingBox4);
        dragLayout.addText(WinText);
        dragLayout.addMenu(Menu);
        dragLayout.addNext(Next);
        dragLayout.addRestart(Restart);
        dragLayout.addCircle(floatingCircle);
//        dragLayout.addDragView(floatingShape5);
//        dragLayout.addDragView(floatingShape6);
//        dragLayout.addDragView(floatingShape7);

        dragLayout.addDragView(floatingShape8);
        dragLayout.addGreenSet(floatingShape8);
        dragLayout.addWinS(WinSearch);
        dragLayout.addDragView(floatingShape0);
        dragLayout.addRainbowSet(floatingShape0);

        dragLayout.addDragView(floatingShape9);
        dragLayout.addRedSet(floatingShape9);
        dragLayout.addDragView(floatingShape1);
        dragLayout.addBlueSet(floatingShape1);
        dragLayout.addGreenV(floatingShape5);
        dragLayout.addRedV(floatingShape7);
        dragLayout.addBlueV(floatingShape6);




        /* Raise the circle in z when the "z+" button is clicked. */
//        rootView.findViewById(R.id.raise_bt).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mElevation += mElevationStep;
//                //Log.d(TAG, String.format("Elevation: %.1f", mElevation));
//                floatingShape.setElevation(mElevation);
//            }
//        });
//
//        /* Lower the circle in z when the "z-" button is clicked. */
//        rootView.findViewById(R.id.lower_bt).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mElevation -= mElevationStep;
//                // Don't allow for negative values of Z.
//                if (mElevation < 0) {
//                    mElevation = 0;
//                }
//                //Log.d(TAG, String.format("Elevation: %.1f", mElevation));
//                floatingShape.setElevation(mElevation);
//            }
//        });

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
       mtimer2.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();

        class firstTask extends TimerTask {

            @Override
            public void run() {
                floatingBox.setRotation(floatingBox.getRotation() + 1);
                if(floatingBox.getRotation()==360)floatingBox.setRotation(floatingBox.getRotation()%360);
                floatingBox2.setRotation(floatingBox2.getRotation()+1);
                if(floatingBox2.getRotation()==360)floatingBox2.setRotation(floatingBox2.getRotation()%360);
                floatingBox3.setRotation(floatingBox3.getRotation()+1);
                if(floatingBox3.getRotation()==360)floatingBox3.setRotation(floatingBox3.getRotation()%360);
                floatingBox4.setRotation(floatingBox4.getRotation()+1);
                if(floatingBox4.getRotation()==360)floatingBox4.setRotation(floatingBox4.getRotation()%360);
                k+=1;
                if(k==2000)
                {
                    DragFrameLayout.testwin=true;

                }
            }
        };
        mtimer2=new Timer();
        BoxTimer= new firstTask();
        mtimer2.schedule(BoxTimer,50,20);

    }

    @Override
    public void onStart() {
        super.onStart();
    }
//    @Override
//    public void onStart() {
//        //super.onResume();  // Always call the superclass method first
//        //mtimer2.schedule(BoxTimer,50,50);
//    }


    /**
     * ViewOutlineProvider which sets the outline to be an oval which fits the view bounds.
     */
    private class CircleOutlineProvider extends ViewOutlineProvider {
        @Override
        public void getOutline(View view, Outline outline) {
            outline.setOval(0, 0, view.getWidth(), view.getHeight());
        }

    }


}